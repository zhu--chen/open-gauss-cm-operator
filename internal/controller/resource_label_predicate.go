/*
Copyright (c) 2023 China Mobile Information Technology Co., Ltd
OpenGauss Operator is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
*/

package controller

import (
	"sigs.k8s.io/controller-runtime/pkg/event"
)

var OperatorName string

func SetOperatorName(s string) {
	OperatorName = s
}

type ResourceLabelPredicate struct {
}

func (r ResourceLabelPredicate) Create(event event.CreateEvent) bool {
	if ContainsLabel(event.Object.GetLabels()) {
		return true
	}
	return false
}

func (r ResourceLabelPredicate) Delete(event event.DeleteEvent) bool {
	if ContainsLabel(event.Object.GetLabels()) {
		return true
	}
	return false
}

func (r ResourceLabelPredicate) Update(event event.UpdateEvent) bool {
	if ContainsLabel(event.ObjectOld.GetLabels()) {
		return true
	}
	return false
}

func (r ResourceLabelPredicate) Generic(event event.GenericEvent) bool {
	if ContainsLabel(event.Object.GetLabels()) {
		return true
	}
	return false
}

func ContainsLabel(m map[string]string) bool {
	if m["operator-name"] == OperatorName {
		return true
	}
	return false
}
