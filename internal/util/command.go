/*
Copyright (c) 2023 China Mobile Information Technology Co., Ltd
OpenGauss Operator is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
*/

package util

const (
	commandStr1 = "/bin/sh"
	commandStr2 = "-c"
)

// gauss db command
const GaussDbCommand = " echo -e \"omm soft nofile 640000\\n omm hard nofile 640000\" >> /etc/security/limits.d/nofile.conf;\n" +
	"echo -e \"root soft nofile 640000\\n root hard nofile 640000\" >> /etc/security/limits.d/nofile.conf;\n" +
	"ulimit -n 640000;\n" +
	"bash /entrypoint.sh; "

// init 创建账号密码、权限授权
const GetInitCommand = "su - omm -c \"" +
	"gs_guc reload -N all -I all -h 'host all all 0.0.0.0/0 sha256';" +
	"gs_guc reload -N all -I all -h 'host all all ::/0 sha256';" +
	"gs_guc reload -N all -I all -h 'host replication all 0.0.0.0/0 sha256';" +
	"gsql -d postgres -p 5432 -c \\\"   " +
	"create user rep1 with sysadmin replication identified by 'sdfg.1314';   " +
	"create user monitor with sysadmin replication identified by 'Test_123';   \\\";" +
	"\""

// 停止数据节点，不会被cma拉起
const CommandStopDn = "su - omm -c 'cm_ctl stop -n ${1} -D /opengauss/cluster/datanode/dn1' "

// 获取数据节点NodeId
const CommandGetDataNodeId = "su - omm -c \" " +
	"    cm_ctl query -Cv | grep '|' |tr '|' '\\n' | grep '${1}'  " +
	"\" | awk '{print$1}' "

// 启动数据节点
const CommandStartDn = "su - omm -c 'cm_ctl start -n ${1} -D /opengauss/cluster/datanode/dn1'"

// 获取实例状态
const CommandGetInstanceStatus = "su - omm -c \" " +
	"    cm_ctl query -Cv | grep '|' |tr '|' '\\n' | grep '${1}'  " +
	"\" | awk '{print$6}' "

// 备机重建
const CommandStandbyReconstruct = "su - omm -c 'cm_ctl build -n ${1} -D /opengauss/cluster/datanode/dn1' "

// 主从切换
const CommandSwitchover = "su - omm -c 'gs_ctl switchover -D /opengauss/cluster/datanode/dn1'"

// 获取集群状态
const CommandGetClusterState = "su - omm -c \"" +
	"cm_ctl query -Cv|grep 'cluster_state'\"|awk '{print $3}'"

// 获取cm_ctl query -Cv信息
const CommandGetClusterMsg = "su - omm -c \"" +
	"cm_ctl query -Cv\""
