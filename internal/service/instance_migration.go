/*
Copyright (c) 2023 China Mobile Information Technology Co., Ltd
OpenGauss Operator is licensed under Mulan PSL v2.
You can use this software according to the terms and conditions of the Mulan PSL v2.
You may obtain a copy of Mulan PSL v2 at:
         http://license.coscl.org.cn/MulanPSL2
THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
See the Mulan PSL v2 for more details.
*/

package service

import (
	"k8s.io/klog/v2"
	gsv1 "openGauss-operator/api/v1"
	customClient "openGauss-operator/internal/client"
	"openGauss-operator/internal/util"
	"reflect"
)

var _ InstanceMigrationInterface = &InstanceMigration{}

type InstanceMigrationInterface interface {
	ReconcileInstanceMigration(gs *gsv1.OpenGaussCluster) error
}

func NewInstanceMigration(client customClient.K8sClient) InstanceMigrationInterface {
	return &InstanceMigration{K8sClient: client}
}

type InstanceMigration struct {
	K8sClient customClient.K8sClient
}

func (im *InstanceMigration) ReconcileInstanceMigration(gs *gsv1.OpenGaussCluster) error {

	instanceMigration := gs.Spec.InstanceMigration
	imStatus := instanceMigration.Status
	migrationPodName := instanceMigration.Instance
	if util.IsEmptyStruct(instanceMigration) || imStatus == util.ImStageSucceed ||
		imStatus == util.ImStageFailed {
		return nil
	}
	modifyGs := gs.DeepCopy()

	if imStatus == util.ImStageCreate {
		// Check the name format

		ord, err := util.CheckPodNameFormat(gs.Name, migrationPodName, int(*gs.Spec.DataNodeNum))
		if err != nil {
			modifyGs.Spec.InstanceMigration.Status = util.ImStageFailed
			modifyGs.Spec.InstanceMigration.Message = err.Error()
			return util.ReturnError("Migration instance format error:", err)
		}

		// Delete pod
		if err := im.K8sClient.DeletePod(migrationPodName, gs.Namespace); err != nil {
			modifyGs.Spec.InstanceMigration.Status = util.ImStageFailed
			modifyGs.Spec.InstanceMigration.Message = err.Error()
			return util.ReturnError("Migration pod delete error:", err)
		}

		// Delete pvc
		pvcName := getPvcName(gs.Name, ord)
		if err := im.K8sClient.DeletePvc(pvcName, gs.Namespace); err != nil {
			modifyGs.Spec.InstanceMigration.Status = util.ImStageFailed
			modifyGs.Spec.InstanceMigration.Message = err.Error()
			return util.ReturnError("Migration pvc delete error:", err)
		}

		// Delete pvc
		pvName := getPvName(gs.Name, ord)
		if err := im.K8sClient.DeletePv(pvName, gs.Namespace); err != nil {
			modifyGs.Spec.InstanceMigration.Status = util.ImStageFailed
			modifyGs.Spec.InstanceMigration.Message = err.Error()
			return util.ReturnError("Migration pv delete error:", err)
		}

		modifyGs.Spec.InstanceMigration.Status = util.ImStageRunning

	}

	if imStatus == util.ImStageRunning {
		// pod running 且 不再原主机，标记成功,在原主机标记失败
		pod, err := im.K8sClient.GetPod(gs.Namespace, migrationPodName)
		if err != nil {
			return util.ReturnError("Migration pod get error  ", err)
		}
		if IsRunningAndReady(pod) {
			modifyGs.Spec.InstanceMigration.Status = util.ImStageSucceed
		}

	}

	// update with instance migration
	if !reflect.DeepEqual(gs.Spec.InstanceMigration, modifyGs.Spec.InstanceMigration) {
		if err := im.K8sClient.UpdateOpenGaussClusterObject(modifyGs, gs); err != nil {
			klog.Error(util.ReturnError("Migration instance update gs error:", err))
		}
	}

	return nil
}
